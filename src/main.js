import Vue from 'vue'
import ElementUI from 'element-ui';
import VueScrollTo from 'vue-scrollto'
import 'element-ui/lib/theme-chalk/index.css';
import 'element-ui/lib/theme-chalk/display.css';
import App from './App.vue'
import store from './store/store'


Vue.config.productionTip = false

// var Vue = require('vue')

Vue.use(require('vue-cookies'))
Vue.use(ElementUI);
Vue.use(VueScrollTo)

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
