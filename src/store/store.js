import 'es6-promise/auto'
import Vuex from 'vuex'
import Vue from 'vue'
// import axios from "axios";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {

    // wsserverurl: 'ws://192.168.1.202:8000/v0/ws/',
    // restserver: 'http://192.168.1.202:8000/v0/',
    wsserverurl: 'wss://apiv2.18node.info/v0/ws/',
    restserver: 'https://apiv2.18node.info/v0/',
    selectedImg: {},
    joinmsgs: {},
    nguid: "",
    client: {},
    lastmsg: {},
    newmsg: {},
    askmsg: '',
    start: false,
  },

  getters: {
    getJoinMsg: state => {
      return state.joinmsgs
    },
    getNguid: state => {
      return state.nguid
    },
    getChatuuid: state => {
      return state.selectedImg.Timeid
    },
    getSelectedImg: state => {
      return state.selectedImg
    },
    getStart: state => {
      return state.start
    }


  },

  mutations: {
    makeSelectedImg(state, img) {
      state.selectedImg = img
    },
    makeStart(state, selected) {
      state.selectedImg = selected
      state.start = true

    },
    addjoinmsg(state, jmsg) {

      state.joinmsgs = jmsg

    },
    setNguid(state, nguid) {

      state.nguid = nguid
    },
    setLastmsg(state, msg) {

      state.lastmsg = msg
    },

    setAskMsg(state, msg) {
      state.askmsg = msg
    },
    endStart(state) {
      state.start = false
    }


  },

  actions: {

    setSelectedImg(context, img) {

      context.commit('makeSelectedImg', img)

    },

    setStart(context, selected) {

      context.commit('makeStart', selected)

    },
    newJoin(context, jmsg) {

      context.commit('addjoinmsg', jmsg)

    },
    setNguid(context, nguid) {
      context.commit('setNguid', nguid)

    },
    setLastmgs(context, msg) {
      context.commit('setLastmsg', msg)

    },
    setAskMsg(context, msg) {
      context.commit('setAskMsg', msg)

    },
    endStart(context) {
      context.commit('endStart')

    }


  }
});